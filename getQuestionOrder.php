<?php
/**
 * getQuestionOrder Plugin for LimeSurvey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2015 Denis Chenu <http://sondages.pro>
 * @copyright 2015 Ysthad <http://ysthad.fr>
 * @license AGPL v3
 * @version 1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
class getQuestionOrder extends PluginBase
{
    protected $storage = 'DbStorage';

    static protected $name = 'getQuestionOrder';
    static protected $description = 'Fill a question with the order of question after randomiztion';

    protected $settings = array(
        'questionordercode' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with order of question.',
            'default' => 'getOrderQ'
        ),
        'questionorderseparator' => array(
            'type' => 'string',
            'label' => 'The separator for question code.',
            'default' => ';',
            'htmlOptions'=>array(
              'maxlength'=>"3",
              'size'=>"4",
            ),
        ),
        'groupordercode' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with order of groups.',
            'default' => 'getOrderG'
        ),
        'grouporderseparator' => array(
            'type' => 'string',
            'label' => 'The separator for group number.',
            'default' => ';',
            'htmlOptions'=>array(
              'maxlength'=>"3",
              'size'=>"4",
            ),
        ),
    );

    public function init() {
        $this->subscribe('beforeSurveyPage');
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');
    }

    public function beforeSurveySettings()
    {
        $event = $this->event;
        $event->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'active'=>array(
                    'type'=>'select',
                    'label'=>'Use getQuestionOrder infor plugin',
                    'options'=>array(
                        '0'=>gt("No"),
                        '1'=>gt("Yes"),
                    ),
                    'current' => $this->get('active', 'Survey', $event->get('survey'),1),
                ),
            )
        ));
    }
    public function newSurveySettings()
    {
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value)
        {
            /* In order use survey setting, if not set, use global, if not set use default */
            $default=$event->get($name,null,null,isset($this->settings[$name]['default'])?$this->settings[$name]['default']:NULL);
            $this->set($name, $value, 'Survey', $event->get('survey'),$default);
        }
    }

    public function beforeSurveyPage()
    {
        $oEvent=$this->getEvent();
        $iSurveyId=$oEvent->get('surveyId');
        
        $oSurvey=Survey::model()->findByPk($iSurveyId);
        $sessionSurvey=Yii::app()->session["survey_{$iSurveyId}"];
        
        if($oSurvey && $this->get('active', 'Survey', $iSurveyId,1) && !isset($sessionSurvey['getQuestionOrder']))
        {
            $sQuestionCode=$this->get('questionordercode',null,null,$this->settings['questionordercode']['default']);
            $oQuestionOrder=Question::model()->find("sid=:sid and title=:title and parent_qid=0",array(':sid'=>$iSurveyId,':title'=>$sQuestionCode));
            if($oQuestionOrder)
            {
                $sQuestionSGQ="{$oQuestionOrder->sid}X{$oQuestionOrder->gid}X{$oQuestionOrder->qid}";
                $iSrid=!empty($sessionSurvey['srid']) ? $sessionSurvey['srid'] : null;
                if(isset($sessionSurvey['fieldmap']))
                {
                  // Get the question order
                  $aQuestion=array();
                  foreach($sessionSurvey['fieldmap'] as $aField)
                  {
                    if(!empty($aField['qid']))
                      $aQuestion[$aField['qid']]=$aField['title'];
                  }
                  // Get the order
                  $sQuestionsOrder=implode($this->get('questionorderseparator',null,null,$this->settings['questionorderseparator']['default']),$aQuestion);
                  // Put in in survey session
                  $sessionSurvey[$sQuestionSGQ]=$sQuestionsOrder;
                  Yii::app()->session["survey_{$iSurveyId}"]=$sessionSurvey;
                  //Save in DB
                  if($iSrid)
                  {
                    $oResponse=Response::model($iSurveyId)->find("id=:srid",array(":srid"=>$iSrid));
                    if($oResponse)
                    {
                      $oResponse->$sQuestionSGQ=$sQuestionsOrder;
                      $oResponse->save();
                    }
                  }
                }
            }
            $sQuestionCode=$this->get('groupordercode',null,null,$this->settings['groupordercode']['default']);
            $oQuestionOrder=Question::model()->find("sid=:sid and title=:title and parent_qid=0",array(':sid'=>$iSurveyId,':title'=>$sQuestionCode));
            if($oQuestionOrder)
            {
                $sQuestionSGQ="{$oQuestionOrder->sid}X{$oQuestionOrder->gid}X{$oQuestionOrder->qid}";
                $sessionSurvey=Yii::app()->session["survey_{$iSurveyId}"];
                $iSrid=!empty($sessionSurvey['srid']) ? $sessionSurvey['srid'] : null;
                if(isset($sessionSurvey['groupReMap']) && !isset($sessionSurvey['getQuestionOrder']))
                {
                  // Get the question order
                  $sGroupOrder=implode($this->get('grouporderseparator',null,null,$this->settings['grouporderseparator']['default']),$sessionSurvey['groupReMap']);
                  // Put in in survey session
                  $sessionSurvey[$sQuestionSGQ]=$sGroupOrder;
                  //Save in DB
                  if($iSrid)
                  {
                    $oResponse=Response::model($iSurveyId)->find("id=:srid",array(":srid"=>$iSrid));
                    if($oResponse)
                    {
                      $oResponse->$sQuestionSGQ=$sGroupOrder;
                      $oResponse->save();
                    }
                  }
                }
            }
            if(isset($sessionSurvey['fieldmap']) && !isset($sessionSurvey['getQuestionOrder']))
            {
              if($oSurvey->active!="Y")
              {
                $sessionSurvey['getQuestionOrder']=true;
              }
              elseif(isset($oResponse))
              {
                $sessionSurvey['getQuestionOrder']=true;
              }
            }
            Yii::app()->session["survey_{$iSurveyId}"]=$sessionSurvey;
        }

    }
}
